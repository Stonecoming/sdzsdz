package com.service.sdzbitbucket.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-04-27T15:39:34.596Z")

@RestSchema(schemaId = "sdzbitbucket")
@RequestMapping(path = "/sdzBitbucket", produces = MediaType.APPLICATION_JSON)
public class SdzbitbucketImpl {

    @Autowired
    private SdzbitbucketDelegate userSdzbitbucketDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userSdzbitbucketDelegate.helloworld(name);
    }

}
